# Solid Studio

A Smart contract management system that makes easy the interaction, exploration and visualisation of smart contracts deployed to different networks.

- Minimalistic
- Simple
- Elegant
- High Performance

![Solid Studio](https://github.com/solid-studio/solid.docs/blob/master/images/new-design.png)

## Motivation

Let's be honest, accessing the Blockchain is difficult, you need to install geth, find your credentials, attach to an rpc endpoint and then start typing web3 commands. This process repeats over and over again, also the problem is that your scripts won't be there the next time you try to attach again, we call this: `geth pain`.

Additionaly, if you want to execute smart contract functions or check the storage of a contract? Can you make it with existing tools? Probably not. 

But we can do it better and that is why we are creating Solid.

The next generation of smart contract tools to make the exploration and inspection of smart contracts a seamless experience.

Discover the hidden world of smart contracts with Solid.

## Features

- Manage multiple Blockchain connections
- Interact with smart contracts deployed in different networks 
- Visualise storage 
- Explore smart contract relationships and inheritance 

## Roadmap

### December 2019

Complete Smart Contract Interaction

Allow Edit contract definition files and connection names

Include utility tools for smart contract interactions (universal encoding and magic generator for mock data)

### January 2020

Improve smart contract instances view

Control flow graph visualisation and exploration (react-ethereum-components)

### February 2019

Storage visualiser (react-ethereum-components)

Filtering and search of smart contracts and connections (Nice to have)

## Install

To run this project locally, please refer to the [Installation instructions](https://github.com/solid-studio/solid.studio/wiki/Install)

## Architecture
